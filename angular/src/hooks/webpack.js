let ManifestPlugin = require('webpack-manifest-plugin');

module.exports = function (generatedWebpackConfig, options) {
    generatedWebpackConfig.plugins.push(new ManifestPlugin());
    return generatedWebpackConfig;
};