<?php
/**
 * Created by PhpStorm.
 * User: mrohmer
 * Date: 10.10.18
 * Time: 17:51
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AngularController extends Controller
{

    public function index() {
        return $this->render("base.html.twig");
    }
}