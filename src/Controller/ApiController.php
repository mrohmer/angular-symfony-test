<?php
/**
 * Created by PhpStorm.
 * User: mrohmer
 * Date: 10.10.18
 * Time: 17:51
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{

    public function index() {
        return new JsonResponse();
    }
}